import 'package:flutter/material.dart';
import 'configuracao.dart';

class Menu extends StatelessWidget{

  Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(
          title: Text('Menu'),
        ),
      body: Center(
        child: InkWell(
          child: Text('Configuração'), onTap: (){
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Configuracao()),
            );
          },
        ),
      ),
    );
  }
}
